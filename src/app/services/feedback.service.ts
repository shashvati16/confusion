import { Injectable } from '@angular/core';
import { Feedback} from '../shared/feedback';
import { Http, Response } from '@angular/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHttpmsgService } from './process-httpmsg.service';
import { RestangularModule, Restangular } from 'ngx-restangular';

import { Observable } from 'rxjs/Observable';


@Injectable()
export class FeedbackService {

  constructor(private restangular: Restangular, private processHTTPmsgService:ProcessHttpmsgService) { }

  submitFeedback(feedback: Feedback): Observable<Feedback>{
    return this.restangular.all("feedback").post(feedback)
       .catch(error =>{ return this.processHTTPmsgService.handleError(error); });
  }


}
