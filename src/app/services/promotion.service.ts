import { Injectable } from '@angular/core';
import { Promotion } from '../shared/promotion';
import { Http, Response } from '@angular/http';
import { PROMOTIONS } from '../shared/promotions';

import { baseURL } from '../shared/baseurl';
import { ProcessHttpmsgService } from './process-httpmsg.service';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/of'
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/delay' ;

@Injectable()
export class PromotionService {

  constructor(private http: Http, private processHTTPmsgService: ProcessHttpmsgService) { }

  getPromotions(): Observable<Promotion[]> {
    return this.http.get(baseURL + 'promotions')
    .map(res => { return this.processHTTPmsgService.extractData(res); })
    .catch(error =>{ return this.processHTTPmsgService.handleError(error); });
  }
  getPromotion(id: number) : Observable<Promotion> {
    return this.http.get(baseURL + 'promotions/'+ id)
    .map(res => { return this.processHTTPmsgService.extractData(res); })
    .catch(error =>{ return this.processHTTPmsgService.handleError(error); });
  }

  getFeaturedPromotion(): Observable<Promotion> {
    return this.http.get(baseURL + 'promotions?featured=true')
    .map(res => { return this.processHTTPmsgService.extractData(res)[0]})
    .catch(error =>{ return this.processHTTPmsgService.handleError(error); });
  }

}
