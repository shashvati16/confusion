import { Injectable } from '@angular/core';
import { Leader } from '../shared/leader';
import { Http, Response } from '@angular/http';
import { LEADERS } from '../shared/leaders';

import { baseURL } from '../shared/baseurl';
import { ProcessHttpmsgService } from './process-httpmsg.service';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/of'
import 'rxjs/add/operator/delay' ;

@Injectable()
export class LeaderService {

  constructor(private http: Http, private processHTTPmsgService: ProcessHttpmsgService) { }

  getLeaders(): Observable<Leader[]>{
    return this.http.get(baseURL + 'leaders')
    .map(res => { return this.processHTTPmsgService.extractData(res); })
    .catch(error =>{ return this.processHTTPmsgService.handleError(error); });
  }

  getLeader(id: number): Observable<Leader>{
    return this.http.get(baseURL + 'leaders/'+ id)
    .map(res => { return this.processHTTPmsgService.extractData(res); })
    .catch(error =>{ return this.processHTTPmsgService.handleError(error); });

  }

  getFeaturedLeader(): Observable<Leader>{
    return this.http.get(baseURL + 'leaders?featured=true')
    .map(res => { return this.processHTTPmsgService.extractData(res)[0]})
    .catch(error =>{ return this.processHTTPmsgService.handleError(error); });
  }
}
