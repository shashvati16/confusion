import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Comment,dishComment } from '../shared/comment'
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { visibility, flyInOut, expand } from '../animations/app.animation';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
  animations: [
    visibility(),
    flyInOut(),
    expand()
  ]
})
export class DishdetailComponent implements OnInit {

  errMess: string;
  commentsForm: FormGroup;
  comment: Comment;
  dish : Dish;
  dishcopy = null;
  commentDate: Date;
  dishIds: number[];
  prev: number;
  next: number;
  visibility = 'shown';

  formErrors = {
    'author' : '',
    'rating' : '',
    'comment': ''
  };
  validationMessages = {
    'author':{
      'required' : 'Author is required.',
      'minlength' : 'Author must be at least 2 characters long',
      'maxlength' : 'Author cannot be more than 25 characters long'
    },
    'comment':{
      'required' : 'Comment is required.',
    },
  };

  constructor(private dishservice: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('BaseURL') private BaseURL) { }

  ngOnInit() {

    this.createForm();
    this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
    this.route.params
    .switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishservice.getDish(+params['id']); })
      .subscribe(dish => { this.dish = dish; this.dishcopy= dish; this.setPrevNext(dish.id); this.visibility = 'shown'; }, errMess => this.errMess = <any>errMess);
  }
  createForm(){
    this.commentsForm = this.fb.group({
      author : '',
      rating : 5,
      comment: ''
    });

    this.commentsForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();

  }
  onValueChanged(data?: any){
    if(!this.commentsForm) { return; }
      const form = this.commentsForm;
      for(const field in this.formErrors){
        this.formErrors[field] = '';
        const control = this.commentsForm;
        if (control && control.dirty && !control.valid){
          const messages = this.validationMessages[field];
          for(const key in control.errors){
            this.formErrors[field] += messages[key] + ' ';
          }
        }
      }
  }
  onSubmit(){
    this.comment = this.commentsForm.value;
    this.comment.date = Date.now().toLocaleString();
    console.log(this.commentsForm);
    this.commentDate = new Date();
    this.comment.date = this.commentDate.toDateString();
    this.dish.comments.push(this.comment);
    this.dishcopy.save()
      .subscribe(dish => this.dish = dish);
    this.commentsForm.reset({
      author : '',
      rating : 5,
      comment: ''
    });
  }
  setPrevNext(dishId: number){
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }
}
